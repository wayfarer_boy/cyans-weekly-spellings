/*
 * @depend vendor/bootstrap.min.js
 * @depend mustache.js
 * @depend howler.js
 * @depend jquery.cookie.js
 * @depend plugins.js
 */

(function ($, window, document, undefined) {

  var Spellings = Spellings || {
    
    questions: [],
    curWord: 0,
    template: 'js/question.mustache',
    qList: 'js/questions.json',
    extId: 'ihcfffemcpehfpikfhdliacnlfjmblmm',
    panels: [],
    wrapper: null,
    answers: [[],[]],
    quotes: [],
    sounds: {},
    started: false,
    resultsWords: ['Perfect!', 'Awesome!', 'Great!', 'Pretty good.', 'Not bad.'],
    timer: 0,
    startTime: 0,
    record: { },
    touch: null,
    id: null,
    
    init: function() {
      var qList = {questions:[]},
          total, i;
      Spellings.touch = $('html').hasClass('touch');
      $.ajax({url: Spellings.qList,dataType:'json',success: function(json) {
        Spellings.id = json.id+json.max;
        Spellings.initRecord(Spellings.id);
        Spellings.initQuotes(json.quotes);
        total = Math.min(json.questions.length, json.max);
        $.each(Spellings.shuffle(json.questions), function(k,v) {
          if (k < json.max) {
            qList.questions.push({
              percentage: (k+1) / (total+1) * 100, 
              word: v.word, 
              number: k+1, 
              total: total
            });
            Spellings.questions.push(v);
          }
        });
        Spellings.initAudio(function() {
          Spellings.setWelcome();
          $.ajax({url: Spellings.template,dataType:'html',success: function(template) {
            Spellings.wrapper = $('div.main-content').append(Mustache.render(template, qList));
            Spellings.wrapper.find('.spelling-page.ending,.spelling-page.results-page').appendTo(Spellings.wrapper);
            Spellings.disableEnter();
            Spellings.panels = Spellings.wrapper.children();
            Spellings.startAnimation();
            $('button.start').click(Spellings.firstQuestion);
            Spellings.wrapper.sizePanel();
            $(window).resize(function() {
              var self = Spellings.wrapper;
              if (self.data('resize')) clearTimeout(self.data('resize'));
              self.data('resize', setTimeout(function() {
                self.sizePanel();
              }, 500));
            });
            $('.main-content,footer').addClass('active');
            $('.navbar .fa-spinner').addClass('hidden');
          }});
        });
      }});
    },

    initAudio: function(callback) {
      var sprite = {};
      Spellings.sounds = {
        intro: new Howl({ urls:['media/intro.ogg', 'media/intro.mp3'],loop:true,volume:0 }),
        bass: new Howl({ urls:['media/loop.ogg','media/loop.mp3'], loop:true }),
        correct: new Howl({urls: ['media/correct.ogg','media/correct.mp3'] }),
        incorrect: new Howl({urls: ['media/incorrect.ogg','media/incorrect.mp3'], volume: 0.5 }),
        start: new Howl({urls: ['media/start.ogg','media/start.mp3'], volume: 0.8 }),
        end: new Howl({urls: ['media/end.ogg','media/end.mp3'], volume: 0.8 }),
        words: {}
      };
      $.each(Spellings.questions, function(k,v) {
        sprite[v.word] = [v.start, v.end - v.start];
      });
      Spellings.sounds.words = new Howl({
        urls: ['media/words.ogg', 'media/words.mp3'],
        sprite: sprite
      });
      if (Spellings.touch) {
        Spellings.sounds.intro.volume(0.7).play();
      } else {
        Spellings.sounds.intro.fadeIn(0.7, 2000);
      }
      Spellings.sounds.intro.on('load', callback);
    },

    initQuotes: function(quotes) {
      var total = 4,
          intro = $('.spelling-page.intro .container');
      quotes = Spellings.shuffle(quotes);
      for (i = 0; i < total; i++) {
        Spellings.quotes.push(quotes[i]);
        $('<h1>').text(quotes[i]).appendTo(intro);
      }
    },

    initRecord: function (id) {
      var time = parseFloat($.cookie('timer'+id)),
          score = parseInt($.cookie('score'+id));
      if (!time) {
        $.cookie('timer'+id, 9999, { expires: 7 });
        time = 9999;
      }
      if (!score) {
        $.cookie('score'+id, 0, { expires: 7 });
        score = 0;
      }
      Spellings.record = {
        time: time,
        score: score
      }
    },

    initImages: function (i, callback) {
      $('<img/>').attr('src', 'images/background_'+i+'.jpg').load(function() {
        if (i === 10) {
          callback();
        } else {
          Spellings.initImages(i+1, callback);
        }
      });
    },

    setWelcome: function () {
      var highscore = '';
      if (Spellings.record.score < Spellings.questions.length) {
        highscore = 'Spell more than '+Spellings.record.score+' out of '+Spellings.questions.length+' words correctly to beat your high score!';
      } else {
        highscore = 'Spell all '+Spellings.questions.length+' words in under '+Spellings.record.time+' seconds to set a new high score!';
      }
      $('p.highscore').text(highscore);
    },

    startTimer: function() {
      var curTime = new Date().getTime();
      Spellings.startTime = curTime;
    },

    endTimer: function() {
      var curTime = new Date().getTime();
      Spellings.timer = parseFloat(((curTime - Spellings.startTime) / 1000.0).toFixed(2));
    },

    startAnimation: function() {
      var jumbo = $('.spelling-page').first(),
          button = $('button', jumbo);
      button.data('interval', setInterval(function() {
        button.toggleClass('animated tada');
      }, 2000));
    },

    stopAnimation: function() {
      clearInterval($('.spelling-page button').data('interval'));
    },

    firstQuestion: function () {
      $(this).button('loading');
      Spellings.stopAnimation();
      Spellings.activePanel = $('.spelling-page').eq(0);
      if (Spellings.touch) {
        Spellings.sounds.intro.stop();
        Spellings.started = true;
        Spellings.activePanel.next().panelActivate();
      } else {
        Spellings.sounds.intro.fadeOut(0, 1000, function() {
          Spellings.sounds.intro.stop();
          Spellings.started = true;
          Spellings.activePanel.next().panelActivate();
        });
      }
    },

    runIntro: function() {
      var panel = Spellings.activePanel.next().removeClass('hidden').addClass('active'),
          quotes = panel.find('h1');
      Spellings.sounds.start.volume(0.7).play();
      setTimeout(function() {
        Spellings.activePanel = panel;
        quotes.each(function(i) {
          var self = $(this);
          setTimeout(function() {
            Spellings.setBodyColour(i+1);
            self.addClass('active');
            setTimeout(function() {
              self.addClass('finish');
            }, 4000);
            if (i === quotes.length-1) {
              setTimeout(function() {
                Spellings.setBodyColour();
                Spellings.sounds.bass.volume(0.5).play();
                panel.next().panelActivate();
                Spellings.startTimer();
              }, 4000);
            }
          }, 4200*i);
        });
      }, 500);
    },

    runEnding: function() {
      var panel = Spellings.activePanel.next().removeClass('hidden').addClass('active'),
          quotes = panel.find('h1');
      Spellings.endTimer();
      Spellings.setResults();
      Spellings.sounds.bass.stop();
      Spellings.endSequence(panel, quotes);
    },

    endSequence: function (panel, quotes) {
      Spellings.sounds.end.volume(0.6).play();
      setTimeout(function() {
        Spellings.activePanel = panel;
        quotes.each(function(i) {
          var self = $(this);
          setTimeout(function() {
            Spellings.setBodyColour(i+1);
            self.addClass('active');
            if (i === quotes.length-1) {
              setTimeout(function() {
                Spellings.setBodyColour();
                if (Spellings.touch) {
                  Spellings.sounds.intro.volume(0.7).play();
                } else {
                  Spellings.sounds.intro.fadeIn(0.7, 1000);
                }
                panel.next().panelActivate();
              }, 4000);
            } else {
              setTimeout(function() {
                self.addClass('finish');
              }, 600);
            }
          }, 750*i);
        });
      }, 800);
    },

    setBodyColour: function (index) {
      var body = $('body');
      body.removeClass('color-1 color-2 color-3 color-4 color-5')
      if (typeof index !== 'undefined') body.addClass('color-'+(index%6));
    },

    setResults: function() {
      var correct = Spellings.answers[1].length,
          total = Spellings.questions.length,
          newRecord = '';
      if (total - correct < Spellings.resultsWords.length) {
        newRecord = Spellings.resultsWords[total - correct];
      }
      if (Spellings.record.score <= correct) {
        $.cookie('score'+Spellings.id, correct);
        if (Spellings.record.score < correct) {
          $.cookie('timer'+Spellings.id, Spellings.timer);
          newRecord += ' A new record!';
        } else if (Spellings.record.timer < Spellings.timer) {
          $.cookie('timer'+Spellings.id, Spellings.timer);
          newRecord += ' A new personal best!';
        }
      }
      $('h1.results')
        .find('.score')
        .text(correct)
        .end()
        .find('.total')
        .text(total);
      $('.results-page')
        .find('.time')
        .text(Spellings.timer)
        .end()
        .find('h1.results')
        .append(' <small>'+newRecord+'</small>');
    },

    validate: function(e) {
      e.preventDefault();
      var panel = Spellings.activePanel,
          self = panel.find('input'),
          button = panel.find('button.ok'),
          val = self.val().toLowerCase().trim(),
          answer = Spellings.activePanel.attr('data-word');
      if (val === '') {
        self.parent().addClass('has-error animated shake');
      } else {
        button.button('loading');
        self.parent().removeClass('has-error animated shake');
        setTimeout(function() {
          Spellings.setState(val, answer, function() {
            Spellings.curWord++;
            Spellings.activePanel.panelActivate();
          });
        }, 1000);
      }
      return false;
    },

    setState: function(val, answer, callback) {
      var panel = Spellings.activePanel,
          title = $('h2', panel),
          button = $('button.ok', panel),
          progress = $('.progress-bar', panel),
          correct = val === answer;
      if (correct) {
        title.prepend('<i class="text-success fa fa-thumbs-up animated bounceIn"></i> ');
        button.button('correct');
        button.prepend('<i class="fa fa-check-square animated bounceIn"></i> ')
          .addClass('btn-success');
        progress.addClass('progress-bar-success');
        Spellings.sounds.correct.play();
        Spellings.answers[1].push([val,answer]);
      } else {
        title.prepend('<i class="text-danger fa fa-thumbs-down animated bounceIn"></i> ');
        button.button('incorrect');
        button.prepend('<i class="fa fa-times animated bounceIn"></i> ')
          .addClass('btn-danger');
        progress.addClass('progress-bar-danger');
        Spellings.sounds.incorrect.play();
        Spellings.answers[0].push([val,answer]);
      }
      setTimeout(callback, 1500);
    },

    say: function(word) {
      console.log(word);
      Spellings.sounds.words.play(word);
    },

    shuffle: function (array) {
      var currentIndex = array.length,
          temporaryValue,
          randomIndex;
      // While there remain elements to shuffle...
      while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    },

    disableEnter: function () {
      $(document).on("keypress", 'form', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
          e.preventDefault();
          Spellings.validate(e);
          return false;
        }
      });
    }

  };

  $(document).ready(function () {
    $('.volume-control').volumeToggle();
    $('button.restart').click(function() { document.location.reload(true); });
    Spellings.init();
  });

  $.fn.sizePanel = function () {
    var content = $(this).css('height', '').css('width', ''),
        h = $(window).height() - $('body').outerHeight(),
        jumbos = content.children('.spelling-page').not('.spelling-word').css('width', ''),
        jumbo = jumbos.first(),
        w = $(window).width() - (jumbo.outerWidth() - jumbo.width());
    jumbos.width(w);
    if (h > 0) {
      content
        .height(h+content.innerHeight())
        .width($(window).width());
    }
  };

  $.fn.panelActivate = function () {
    var activePanel = $('.spelling-page.active').last(),
        self = $(this),
        word = self.attr('data-word');
    if (activePanel.length > 0) {
      activePanel
        .removeClass('bounceInRight active')
        .addClass('bounceOutLeft');
      setTimeout(function() {
        activePanel.addClass('hidden');
        activePanel.next().panelActivate();
      }, 500);
    } else {
      if (self.attr('data-word')) {
        Spellings.activePanel = self
          .removeClass('hidden')
          .addClass('bounceInRight active');
        Spellings.setBodyColour(Spellings.activePanel.index());
        setTimeout(function () {
          self 
            .find('button.repeat')
            .click(function() {
              Spellings.say(word);
              self.find('input').trigger('focus').trigger('click');
            })
            .end()
            .find('button.ok')
            .click(Spellings.validate)
            .end()
            .find('input')
            .submit(Spellings.validate)
            .trigger('focus')
            .trigger('click');
          setTimeout(function() {
            Spellings.say(word);
          }, 500);
        }, 500); 
      } else if (self.hasClass('intro')) {
        Spellings.runIntro();
      } else if (self.hasClass('ending')) {
        Spellings.runEnding();
      } else {
        Spellings.activePanel = self
          .removeClass('hidden')
          .addClass('bounceInRight active');
      }
    }
  };

  $.fn.volumeToggle = function() {
    var self = $(this),
        icon = $('i', self);;
    self.click(function() {
      if (self.hasClass('active')) {
        Howler.unmute();
      } else {
        Howler.mute();
      }
      self.toggleClass('active');
      icon.toggleClass('fa-volume-off fa-volume-up');
    });
    /*self.trigger('click');*/
  };

})($, this, this.document);
